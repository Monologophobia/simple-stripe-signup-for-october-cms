<?php

// Static route for Stripe Webhooks
// point your webhooks to https://your-url.tld/stripe/webhooks
Route::post('stripe/webhooks/', 'Monologophobia\Signup\Components\StripeWebhooks@handleWebhook');