<?php namespace Monologophobia\Signup\Updates;

use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class PlanCategories extends Migration {

    public function up() {

        Schema::create('mono_signup_plans_categories', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
        });

        DB::table('mono_signup_plans_categories')->insert(['id' => 1, 'name' => 'Default']);

        // create a separate table to avoid any future upstream changes
        Schema::table('mono_signup_plans', function($table) {
            $table->integer('category_id')->unsigned()->default(1);
            $table->foreign('category_id')->references('id')->on('mono_signup_plans_categories')->onDelete('cascade');
        });

    }

    public function down() {

        Schema::table('mono_signup_plans', function($table) {
            $table->dropColumn('category_id');
        });

        Schema::dropIfExists('mono_signup_plans_categories');

    }

}
