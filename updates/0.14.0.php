<?php namespace Monologophobia\Signup\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ZeroPointFourteenPointZero extends Migration {

    public function up() {

        Schema::create('mono_signup_marketing_categories', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('mono_signup_user_marketing_categories', function($table) {
            $table->integer('user_id')->index()->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('category_id')->index()->unsigned();
            $table->foreign('category_id')->references('id')->on('mono_signup_marketing_categories')->onDelete('cascade');
        });

    }

    public function down() {
        Schema::dropIfExists('mono_signup_user_marketing_categories');
        Schema::dropIfExists('mono_signup_marketing_categories');
    }

}
