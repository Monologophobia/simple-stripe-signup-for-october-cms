<?php namespace Monologophobia\Signup\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddPlans extends Migration {

    public function up() {

        Schema::create('mono_signup_plans', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->decimal('amount', 9, 2);
            $table->integer('recurring_frequency')->default(0);
            $table->integer('interval_count')->default(1);
            $table->integer('trial_period_days')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        // create a separate table to avoid any future upstream changes
        Schema::table('mono_users_stripe', function($table) {
            $table->integer('plan_id')->unsigned();
            // create a database event to automatically clean this table on deletion of a plan
            $table->foreign('plan_id')->references('id')->on('mono_signup_plans')->onDelete('cascade');
        });

    }

    public function down() {

        Schema::table('mono_users_stripe', function($table) {
            $table->dropColumn('plan_id');
        });

        Schema::dropIfExists('mono_signup_plans');

    }

}
