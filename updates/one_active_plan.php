<?php namespace Monologophobia\Signup\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class OneActivePlan extends Migration {

    public function up() {

        Schema::table('mono_signup_plans_categories', function($table) {
            $table->boolean('one_active_plan')->default(false);
        });

    }

    public function down() {

        Schema::table('mono_signup_plans_categories', function($table) {
            $table->dropColumn('one_active_plan');
        });

    }

}
