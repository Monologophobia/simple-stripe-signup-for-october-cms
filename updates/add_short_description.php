<?php namespace Monologophobia\Signup\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddShortDescription extends Migration {

    public function up() {
        Schema::table('mono_signup_plans', function($table) {
            $table->text('short_description')->nullable();
        });
    }

    public function down() {
        Schema::table('mono_signup_plans', function($table) {
            $table->dropColumn('short_description');
        });
    }

}
