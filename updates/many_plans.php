<?php namespace Monologophobia\Signup\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ManyPlans extends Migration {

    public function up() {

        Schema::create('mono_users_plans', function($table) {
            $table->integer('user_id')->unsigned();
            $table->integer('plan_id')->unsigned();
        });

        Schema::table('users', function($table) {
            $table->dropColumn('plan_id');
        });
    }

    public function down() {

        Schema::table('users', function($table) {
            $table->integer('plan_id')->nullable();
        });

        Schema::dropIfExists('mono_users_plans');

    }

}
