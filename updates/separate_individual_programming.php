<?php namespace Monologophobia\Signup\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class IndividualProgramming extends Migration {

    public function up() {

        Schema::create('mono_users_individual_programming', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->date('active_date');
            $table->text('monday')->nullable();
            $table->text('tuesday')->nullable();
            $table->text('wednesday')->nullable();
            $table->text('thursday')->nullable();
            $table->text('friday')->nullable();
            $table->text('saturday')->nullable();
            $table->text('sunday')->nullable();
            $table->timestamps();
        });

        Schema::table('users', function($table) {
            $table->renameColumn('individual_programming', 'old_individual_programming');
        });

    }

    public function down() {

        Schema::dropIfExists('mono_users_individual_programming');

        Schema::table('users', function($table) {
            $table->renameColumn('old_individual_programming', 'individual_programming');
        });

    }

}
