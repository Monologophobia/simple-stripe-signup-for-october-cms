<?php namespace Monologophobia\Signup\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ZeroPointThirteenPointZero extends Migration {

    public function up() {

        Schema::table('mono_signup_plans', function($table) {
            $table->text('training_dates')->nullable();
            $table->string('redirect')->nullable();
            $table->decimal('deposit', 9, 2)->default(0.00);
        });

        Schema::table('mono_users_plans', function($table) {
            $table->integer('training_dates')->nullable();
        });

    }

    public function down() {

        Schema::table('mono_signup_plans', function($table) {
            $table->dropColumn('training_dates');
            $table->dropColumn('redirect');
            $table->dropColumn('deposit');
        });

        Schema::table('mono_users_plans', function($table) {
            $table->dropColumn('training_dates');
        });

    }

}