<?php namespace Monologophobia\Signup\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Exercises extends Migration {

    public function up() {

        Schema::create('mono_exercises', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->text('video_link');
            $table->timestamps();
        });

    }

    public function down() {
        Schema::dropIfExists('mono_exercises');
    }

}
