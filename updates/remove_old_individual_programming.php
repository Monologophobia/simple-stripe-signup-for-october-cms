<?php namespace Monologophobia\Signup\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class IndividualProgramming extends Migration {

    public function up() {

        Schema::table('users', function($table) {
            $table->dropColumn('old_individual_programming');
        });

    }

    public function down() {

        Schema::table('users', function($table) {
            $table->json('old_individual_programming')->nullable();
        });

    }

}
