<?php namespace Monologophobia\Signup\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

use RainLab\User\Models\User;

class ZeroPointTwelvePointZero extends Migration {

    public function up() {

        Schema::table('users', function($table) {
            if (!Schema::hasColumn('users', 'postcode')) {
                $table->dropColumn('postcode');
            }
            if (!Schema::hasColumn('users', 'dob')) {
                $table->dropColumn('dob');
            }
            if (!Schema::hasColumn('users', 'postcgenderode')) {
                $table->dropColumn('gender');
            }
        });

        // encrypt any existing names
        $users = User::get();
        foreach ($users as $user) {
            // plugin beforesave takes care of the encryption process
            $user->save();
        }

    }

    public function down() {

        Schema::table('users', function($table) {
            $table->text('gender')->nullable();
            $table->date('dob')->nullable();
            if (!Schema::hasColumn('users', 'postcode')) {
                $table->text('postcode')->nullable();
            }
        });

    }

}
