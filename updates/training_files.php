<?php namespace Monologophobia\Signup\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class TrainingFiles extends Migration {

    public function up() {

        // create a separate table to avoid any future upstream changes
        Schema::table('mono_signup_plans', function($table) {
            $table->json('training_files')->nullable();
        });

    }

    public function down() {

        Schema::table('mono_signup_plans', function($table) {
            $table->dropColumn('training_files');
        });

    }

}
