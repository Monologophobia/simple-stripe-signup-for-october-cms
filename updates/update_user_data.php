<?php namespace Monologophobia\Signup\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateUserData extends Migration {

    public function up() {

        // create a separate table to avoid any future upstream changes
        Schema::table('users', function($table) {
            $table->text('gender')->nullable();
            $table->date('dob')->nullable();
            if (!Schema::hasColumn('users', 'postcode')) {
                $table->text('postcode')->nullable();
            }
            $table->boolean('terms_and_conditions')->default(true);
        });

    }

    public function down() {

        Schema::table('users', function($table) {
            $table->dropColumn('postcode');
            $table->dropColumn('dob');
            $table->dropColumn('gender');
            $table->dropColumn('terms_and_conditions');
        });

    }

}
