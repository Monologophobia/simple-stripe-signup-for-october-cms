<?php namespace Monologophobia\Signup\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class OrderPlans extends Migration {

    public function up() {

        Schema::table('mono_signup_plans', function($table) {
            $table->integer('order')->default(0);
        });

    }

    public function down() {

        Schema::table('mono_users_stripe', function($table) {
            $table->dropColumn('order');
        });

    }

}
