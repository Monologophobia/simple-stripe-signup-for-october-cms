<?php namespace Monologophobia\Signup\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ManyPlansTimestamps extends Migration {

    public function up() {

        Schema::table('mono_users_plans', function($table) {
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });

    }

    public function down() {

        Schema::table('mono_users_plans', function($table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });

    }

}
