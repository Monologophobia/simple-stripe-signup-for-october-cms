<?php namespace Monologophobia\Signup\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class IndividualProgramming extends Migration {

    public function up() {

        Schema::table('users', function($table) {
            $table->text('individual_programming')->nullable();
        });
    }

    public function down() {

        Schema::table('users', function($table) {
            $table->dropColumn('individual_programming');
        });

    }

}
