<?php namespace Monologophobia\Signup\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateTables extends Migration {

    public function up() {
        
        // create a separate table to avoid any future upstream changes
        Schema::create('mono_users_stripe', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('customer_id')->nullable();
            // create a database event to automatically clean this table on deletion of a user
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

    }

    public function down() {

        Schema::dropIfExists('mono_users_stripe');

    }

}
