<?php namespace Monologophobia\Signup\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ExerciseSessions extends Migration {

    public function up() {

        Schema::create('mono_exercise_sessions', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('day_date');
            $table->integer('plan_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });

        Schema::create('mono_exercise_sessions_exercises', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('session_id')->unsigned()->index();
            $table->foreign('session_id')->references('id')->on('mono_exercise_sessions')->onDelete('cascade');
            $table->integer('exercise_id')->unsigned()->index();
            $table->foreign('exercise_id')->references('id')->on('mono_exercises')->onDelete('cascade');
            $table->integer('reps')->default(1);
            $table->integer('sets')->default(1);
            $table->json('additional')->nullable();
            $table->timestamps();
        });

    }

    public function down() {

        Schema::dropIfExists('mono_exercise_sessions_exercises');
        Schema::dropIfExists('mono_exercise_sessions');

    }

}
