<?php namespace Monologophobia\Signup\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

class UsersExport extends Controller {

    public $implement = [
        'Backend.Behaviors.ImportExportController',
    ];

    public $importExportConfig = 'config_export.yaml';

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('RainLab.User', 'user', $this->action);
    }

}