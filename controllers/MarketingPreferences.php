<?php namespace Monologophobia\Signup\Controllers;

use Mail;
use Flash;
use Backend;
use Redirect;

use BackendMenu;
use Backend\Classes\Controller;

use Monologophobia\Signup\Models\MarketingCategory;

class MarketingPreferences extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.RelationController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $requiredPermissions = ['monologophobia.signup'];

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.Signup', 'signup', 'marketingpreferences');
    }

    public function index_onDelete() {
        try {
            if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
                foreach ($checkedIds as $id) {
                    MarketingCategory::findOrFail($id)->delete();
                }
                Flash::success('Bookings deleted.');
            }
        }
        catch (\Exception $e) {
            Flash::error($e->getMessage());
        }
        return $this->listRefresh();
    }

    public function onExport(int $id) {
        return Redirect::to(Backend::url('monologophobia/signup/marketingpreferences/export/' . $id));
    }

    public function export(int $id) {
        try {

            $columns = ['email', 'created_at'];

            $marketing_category = MarketingCategory::findOrFail($id);
            $users = $marketing_category->users;
            $users->each(function($users) use ($columns) {
                $users->addVisible($columns);
            });

            $results = $users->toArray();

            $headers = [
                'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
                'Content-type'        => 'text/csv',
                'Content-Disposition' => 'attachment; filename=export.csv',
                'Expires'             => '0',
                'Pragma'              => 'public'
            ];

            // stream csv output
            $callback = function() use ($results) {
                $fh = fopen('php://output', 'w');
                foreach ($results as $row) {
                    fputcsv($fh, $row);
                }
                fclose($fh);
            };

            return \Response::stream($callback, 200, $headers);

        }
        catch (Exception $e) {
            Flash::error($e->getMessage());
            return Redirect::back();
        }
    }

}
