<?php namespace Monologophobia\Signup\Controllers;

use Lang;
use Flash;
use BackendMenu;
use Backend\Classes\Controller;
use Monologophobia\Signup\Models\Category;

class Categories extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['monologophobia.signup.plans'];

    public $bodyClass  = 'compact-container';

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('RainLab.User', 'user', 'plans');
    }

    public function index_onDelete() {

        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            try {
                foreach ($checkedIds as $id) {
                    $category = Category::findOrFail($id);
                    $category->delete();
                }
                Flash::success(Lang::get('monologophobia.signup::lang.categories.text.deleted'));
            }
            catch (\Exception $e) {
                Flash::error($e->getMessage());
            }

            return $this->listRefresh();

        }

    }

}
