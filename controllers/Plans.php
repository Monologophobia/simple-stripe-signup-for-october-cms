<?php namespace Monologophobia\Signup\Controllers;

use Lang;
use Flash;
use BackendMenu;
use Backend\Classes\Controller;
use Monologophobia\Signup\Models\Plan;

class Plans extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.RelationController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $requiredPermissions = ['monologophobia.signup.plans'];

    public $bodyClass  = 'compact-container';

    protected $exerciseFormWidget;
    
    public function __construct() {
        parent::__construct();
        $this->exerciseFormWidget = $this->createExerciseFormWidget();
        BackendMenu::setContext('RainLab.User', 'user', 'plans');
    }

    public function index_onDelete() {

        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            try {
                foreach ($checkedIds as $id) {
                    $plan = Plan::findOrFail($id);
                    $plan->delete();
                }
                Flash::success(Lang::get('monologophobia.signup::lang.plans.text.deleted'));
            }
            catch (\Exception $e) {
                Flash::error($e->getMessage());
            }

            return $this->listRefresh();

        }

    }

    /**
     * Loads the exercise form to add exercises to an exercise session
     * first checks for the existence of an exercise session (as we're not using
     * deferred binding, one has to exist before we proceed)
     * if one doesn't exist, create it via getExerciseSessionModel with a supplied plan_id
     */
    public function onLoadAddExerciseForm($plan_id) {
        // create a new exercisesession before loading the exercise form
        // as deferred binding no longer exists
        $exercise_session = $this->getExerciseSessionModel($plan_id);
        $this->vars['exerciseFormWidget'] = $this->exerciseFormWidget;
        $this->vars['exercise_session_id'] = $exercise_session->id;
        return $this->makePartial('exercise_create_form');
    }

    protected function createExerciseFormWidget() {
        $config = $this->makeConfig('$/monologophobia/signup/models/exercise/pivot_fields.yaml');
        $config->alias = 'exerciseForm';
        $config->arrayName = 'Exercise';
        $config->model = new \Monologophobia\Signup\Models\Exercise;
        $widget = $this->makeWidget('Backend\Widgets\Form', $config);
        $widget->bindToController();
        return $widget;
    }

    /**
     * Receives data from adding an exercise to an exercise session
     * interprets all the posted data and builds a correct pivot model
     * then adds it to the main exercise session
     */
    public function onAddExercise() {

        $data = $this->exerciseFormWidget->getSaveData();
        $exercise = \Monologophobia\Signup\Models\Exercise::find(intval($data['exercise_id']));

        $pivot_data = [
            'sets'       => intval($data['sets']),
            'reps'       => intval($data['reps']),
            'additional' => json_encode($data['additional']),
            'sort_order' => intval($data['sort_order'])
        ];

        // add an exercise to a session with deferred binding and pivot data
        $exercise_session = $this->getExerciseSessionModel();
        $exercise_session->exercises()->add($exercise, $pivot_data);

        return $this->refreshExerciseList();

    }

    /**
     * Refreshes the list post adding an exercise to an exercise session
     */
    protected function refreshExerciseList() {
        $exercises = $this->getExerciseSessionModel()->exercises()->get();
        $this->vars['exercises'] = $exercises;
        $this->vars['exercise_session_id'] = post('manage_id');
        return ['#exercise-list' => $this->makePartial('exercise_list')];
    }

    /**
     * Get the current exercise session
     * if it doesn't exist, create it and save it so we always have an id to use
     * this is because deferred binding is not being used
     * so we can pass the exercise session id around
     * @param integer optional plan_id to bind this new exercise session to
     */
    protected function getExerciseSessionModel(int $plan_id = null) {
        $id = post('manage_id');
        $exercise_session = $id ? \Monologophobia\Signup\Models\ExerciseSession::find($id) : new \Monologophobia\Signup\Models\ExerciseSession;
        // ensure the exercise session has an id so we don't have to rely on deferred binding
        if (!$exercise_session->exists) {
            // dummy data and morphing status
            $exercise_session->day_date       = post('ExerciseSession[day_date]') ? intval(post('ExerciseSession[day_date]')) : 1;
            $exercise_session->morphable_id   = $plan_id;
            $exercise_session->morphable_type = "Monologophobia\Signup\Models\Plan";
            $exercise_session->save();
        }
        return $exercise_session;
    }

    /**
     * Override the onRelationManageCreate function
     * if we're supplied a manage_id
     * (which has been hacked in via a javascript method)
     * then call update process instead of create
     */
    public function onRelationManageCreate() {
        if (post('manage_id')) {
            return parent::onRelationManageUpdate();
        }
        else {
            return parent::onRelationManageCreate();
        }
    }

    /**
     * Remove an exercise from an exercise session
     */
    public function onRemoveExercise() {
        $pivot_id = intval(post('record_id'));
        \DB::table('mono_exercise_sessions_exercises')->where('id', $pivot_id)->delete();
        return $this->refreshExerciseList();
    }

}
