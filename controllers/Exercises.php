<?php namespace Monologophobia\Signup\Controllers;

use Flash;
use BackendMenu;
use Backend\Classes\Controller;
use Monologophobia\Signup\Models\Exercise;

class Exercises extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['monologophobia.signup'];

    public $bodyClass  = 'compact-container';

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('RainLab.User', 'user', 'exercises');
    }

    public function index_onDelete() {

        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            try {
                foreach ($checkedIds as $id) {
                    $exercise = Exercise::findOrFail($id);
                    $exercise->delete();
                }
                Flash::success(Lang::get('monologophobia.signup::lang.plans.text.deleted'));
            }
            catch (\Exception $e) {
                Flash::error($e->getMessage());
            }

            return $this->listRefresh();

        }

    }

}
