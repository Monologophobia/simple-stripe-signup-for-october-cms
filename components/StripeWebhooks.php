<?php namespace Monologophobia\Signup\Components;

use Log;
use Mail;
use Response;
use Backend\Models\UserGroup;
use RainLab\User\Models\User;
use Illuminate\Routing\Controller;
use Monologophobia\Signup\Models\Settings as SignupSettings;

class StripeWebhooks extends Controller {

    public function handleWebhook() {

        try {

            // init stripe
            $live = boolval(SignupSettings::get('live'));
            $key  = ($live ? SignupSettings::get('stripe_secret') : SignupSettings::get('stripe_sandbox_secret'));
            $webhook_signing_key = ($live ? SignupSettings::get('stripe_webhook_secret') : SignupSettings::get('stripe_sandbox_webhook_secret'));
            \Stripe\Stripe::setApiKey($key);

            $payload = @file_get_contents("php://input");
            $sig_header = $_SERVER["HTTP_STRIPE_SIGNATURE"];

            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $webhook_signing_key
            );

            if (isset($event)) {

                // at this point we only care about payment failures outside of the system
                // eg no funds, fraud, etc
                // Stripe will automatically attempt to take payment again so
                // to start with, just notify the customer and the "owners" group
                if ($event->type == "invoice.payment_failed") {
                    $user = $this->getUser($event->data->object->customer);
                    if ($user) {
                        $this->sendNotifications($user, 'monologophobia.signup::signup.payment_failed');
                    }
                }

                // if the payment continues to fail we will eventually receive
                // a customer.subscription.deleted event
                // (this will also trigger if a customer deletes their account
                // thus entering a race condition but the account deletion `should` win.
                // To make sure of this, the cancelStripeCustomer method also removes
                // their stripe customer_id)
                if ($event->type == "customer.subscription.deleted") {
                    $user = $this->getUser($event->data->object->customer);
                    if ($user) {
                        $this->sendNotifications($user, 'monologophobia.signup::signup.subscription_cancelled');
                        // we could cancel the account here and now but
                        // it's probably best to wait for an admin to do something
                    }
                }

            }

            // report everything's fine
            http_response_code(200);

        }

        // Invalid payload
        catch (\UnexpectedValueException $e) {
            http_response_code(400);
        }

        // Invalid signature
        catch (\Stripe\Error\SignatureVerification $e) {
            http_response_code(400);
        }

        // something else went wrong. Log it and send a 500 error
        catch (\Exception $e) {
            Log::error($e);
            http_response_code(500);
        }

        die();

    }

    private function getUser($customer_id) {
        return User::where('customer_id', $customer_id)->first();
    }

    private function sendNotifications($user, $template) {
        // send email notification to customer
        $vars = ['user' => $user, 'date' => Date('Y-m-d H:i:s')];
        Mail::sendTo($user->email, $template, $vars);
        // and to owners group
        $group = UserGroup::where('code', 'owners')->first();
        foreach ($group->users as $admin) {
            Mail::sendTo($admin->email, $template, $vars);
        }
    }

}

?>
