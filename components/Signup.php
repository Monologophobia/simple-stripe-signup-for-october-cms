<?php namespace Monologophobia\Signup\Components;

use Mail;
use Lang;
use Auth;
use Event;
use Flash;
use Stripe;
use Request;
use Redirect;
use Validator;
use Exception;
use Cms\Classes\Page;
use ValidationException;
use ApplicationException;
use RainLab\User\Models\User;
use Cms\Classes\ComponentBase;
use Monologophobia\Signup\Models\Plan;
use RainLab\User\Models\Settings as UserSettings;
use Monologophobia\Signup\Models\MarketingCategory;
use Monologophobia\Signup\Models\Settings as SignupSettings;

/**
 * Signup component
 *
 * Provides a signup facility including payment plans
 * Respects certain RainLab.User settings, such as 
 * is registration active / redirect url / loginattribute
 * Ignores user activation system... This is probably bad.
 */

class Signup extends ComponentBase {

    public function componentDetails() {
        return [
            'name'        => 'monologophobia.signup::lang.components.signup.name',
            'description' => 'monologophobia.signup::lang.components.signup.description'
        ];
    }

    public function defineProperties() {
        return [
            'plan_id' => [
                'title' => 'monologophobia.signup::lang.components.signup.plan_id',
                'type'  => 'text'
            ],
            'redirect' => [
                'title'       => 'rainlab.user::lang.account.redirect_to',
                'description' => 'rainlab.user::lang.account.redirect_to_desc',
                'type'        => 'dropdown',
                'default'     => ''
            ]
        ];
    }

    public function getRedirectOptions() {
        return [''=>'- refresh page -', '0' => '- no redirect -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun() {

        // get plan if it exists
        $plan_id = intval($this->property('plan_id'));
        if (boolval($plan_id)) {

            $this->page['user'] = Auth::getUser();

            $this->page['plan'] = Plan::where('id', $plan_id)->first();
            if (!$this->page['plan']) {
                Flash::error(Lang::get('monologophobia.signup::lang.components.messages.plan_does_not_exist'));
                return Redirect::back();
            }
        
            // if registrations are not allowed, redirect back with message
            if (!UserSettings::get('allow_registration', true)) {
                Flash::error(Lang::get('rainlab.user::lang.account.registration_disabled'));
                return Redirect::back();
            }

            // get marketing categories
            $this->page['marketing_categories'] = MarketingCategory::get();

            // get correct stripe key for sandbox or live environment
            $live = boolval(SignupSettings::get('live'));
            $this->page['publishable_key'] = ($live ? SignupSettings::get('stripe_publishable') : SignupSettings::get('stripe_sandbox_publishable'));

        }
        // else get all plans
        else {
            $this->page['plans'] = Plan::orderBy('order', 'asc')->get();
        }

        $this->page['url'] = Request::url();

    }

    public function onRegister() {
        try {

            $this->page['plan'] = Plan::where('id', intval($this->property('plan_id')))->first();
            if (!$this->page['plan']) {
                return Flash::error(Lang::get('monologophobia.signup::lang.components.messages.plan_does_not_exist'));
            }

            $data = post();
            if (!Auth::getUser()) $data = $this->validateInputs(post());

            // extensions
            Event::fire('monologophobia.signup.beforeSignup', [$data]);

            // set up a user
            $user = $this->setupUser();

            // if this requires a payment
            if ($this->page['plan']->amount > 0) {

                $this->setupStripe();

                // set up a customer at stripe and store the id
                $customer = $this->setupStripeCustomer($user);
                $user->customer_id = $customer->id;

                // if this is a recurring plan
                if ($this->page['plan']->recurring_frequency > 0) {

                    // set up plan
                    $plan = $this->setupStripePlan();

                    // create the subscription and attach it to the customer
                    $subscription = $this->createStripeSubscription($customer, $user, $plan);

                    if (intval($this->page['plan']->deposit) > 0) {
                        $this->takeSinglePayment($this->page['plan']->deposit * 100, $user, $this->page['plan']->name);
                    }

                }
                // else this is a one off payment
                else {
                    $this->takeSinglePayment($this->page['plan']->amount * 100, $user, $this->page['plan']->name);
                }

            }

            // extensions
            Event::fire('monologophobia.signup.afterSignup', [$user, $data]);

            // if it's a new customer, send them the welcome email
            if (!$user->exists) Mail::sendTo($user->email, 'rainlab.user::mail.welcome', ['name' => $user->name]);

            // complete
            $user->save();
            
            // add plan to user account
            $training_dates = post('training_dates');
            if (isset($training_dates)) {
                $training_dates = intval($training_dates);
                $user->plans()->attach($this->page['plan']->id, ['training_dates' => $training_dates]);
            }
            else {
                $user->plans()->attach($this->page['plan']->id);
            }

            $this->checkOneActivePlanCategory($user, $this->page['plan']);

            // add marketing categories
            if (isset($data['marketing_preferences'])) {
                $user->updateMarketingPreferences($data['marketing_preferences']);
            }

            // auto login user and redirect
            Flash::success(Lang::get('monologophobia.signup::lang.components.messages.signup_successful'));
            Auth::login($user);
            return $this->redirectToPage($this->page['plan']);

        }
        catch (Exception $ex) {
            if (Request::ajax()) throw $ex;
            else Flash::error($ex->getMessage());
        }
    }

    /**
     * Validates data from post
     * @param POST Array
     * @return Array
     * @throws Exception
     */
    private function validateInputs($data) {

        if (!array_key_exists('password_confirmation', $data)) {
            $data['password_confirmation'] = post('password');
        }

        $rules = [
            'email'    => 'required|email|between:6,255',
            'password' => 'required|between:4,255|confirmed'
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        // clean up any marketing preferences that exist
        if (isset($data['marketing_preferences'])) {
            if (is_array($data['marketing_preferences'])) {
                $marketing_preferences = [];
                foreach ($data['marketing_preferences'] as $preference) $marketing_preferences[] = intval($preference);
                $data['marketing_preferences'] = $marketing_preferences;
            }
        }

        return $data;

    }

    /**
     * Redirect to specified page
     * If no page has been specified, refresh
     * @param Plan
     */
    private function redirectToPage($plan = false) {

        if ($plan) {
            if ($plan->redirect) return Redirect::intended($plan->redirect);
        }

        $redirectUrl = $this->pageUrl($this->property('redirect'))
        ?: $this->property('redirect');

        if ($redirectUrl = post('redirect', $redirectUrl)) {
            return Redirect::intended($redirectUrl);
        }

        if (Auth::getUser()) {
            Flash::success(Lang::get('monologophobia.signup::lang.components.messages.already_logged_in'));
            return Redirect::to('/');
        }
        else {
            return Redirect::refresh();
        }

    }

    /**
     * Sets up Stripe with the correct API key
     * Checks if we're live and selects the correct secret key
     */
    private function setupStripe() {
        $live = boolval(SignupSettings::get('live'));
        $key  = ($live ? SignupSettings::get('stripe_secret') : SignupSettings::get('stripe_sandbox_secret'));
        \Stripe\Stripe::setApiKey($key);
    }

    /**
     * Returns the logged in user or
     * Creates a new (or amends an old) user
     * @return User model
     */
    private function setupUser() {
        if ($user = Auth::getUser()) return $user;
        $email = filter_var(post('email'), FILTER_SANITIZE_EMAIL);
        $user = User::where('email', $email)->first();
        if (!$user) $user = new User;
        $user->name         = filter_var(post('name'), FILTER_SANITIZE_STRING);
        $user->surname      = filter_var(post('surname'), FILTER_SANITIZE_STRING);
        $user->email        = $email;
        $user->is_activated = true;
        $user->password     = post('password');
        $user->password_confirmation = post('password_confirmation');
        $user->terms_and_conditions = (post('terms_and_conditions') ? boolval(post('terms_and_conditions')) : true);
        return $user;
    }

    /**
     * Sets up a Stripe customer
     * @param User model
     * @return Stripe Customer object
     */
    private function setupStripeCustomer($user) {

        $customer = false;
        $token    = post('token');

        // if we already have a customer id, make sure that it's valid
        if ($user->customer_id) {
            try {
                if ($customer = \Stripe\Customer::retrieve($user->customer_id)) {
                    $this->checkSource($customer, $token);
                    return $customer;
                }
            }
            catch (\Exception $e) {
                $user->customer_id = false;
            }
        }

        $array = [
            'email'  => $user->email,
            'source' => $token
        ];

        if ($customer) {
            foreach ($array as $key => $value) {
                $customer->$key = $value;
            }
            $customer->save();
        }
        else {
            $customer = \Stripe\Customer::create($array);
        }

        return $customer;

    }

    /**
     * Checks the customer object to make sure they have the correct source
     * listed against their account
     * @param \Stripe\Customer Object
     * @param String token
     */
    private function checkSource($customer, $token) {
        // and make sure the payment source matches the supplied payment data
        try {
            $stripe_token = \Stripe\Token::retrieve($token);
            $match = false;
            foreach ($customer->sources->autoPagingIterator() as $source) {
                if ($source->id == $stripe_token->id) {
                    $match = true;
                    break;
                }
            }
            if (!$match) throw new \Exception("No Source Found");
        }
        catch (\Exception $e) {
            \Log::error($e);
            $customer->source = $token;
            $customer->save();
        }
    }

    /**
     * Checks stripe to see if the plan already exists
     * if it doesn't, create it.
     * @return Stripe Plan object
     */
    private function setupStripePlan() {
        try {
            $plan = \Stripe\Plan::retrieve($this->page['plan']->id);
            return ($plan ? $plan : $this->createStripePlan());
        }
        catch (\Exception $e) {
            return $this->createStripePlan();
        }
    }

    /**
     * Creates the subscription plan
     */
    private function createStripePlan() {
        $array = [
            // amount is a double so * 100 to get pence
            'amount'         => $this->page['plan']->amount * 100,
            'interval'       => $this->page['plan']->getRecurringFrequency(),
            'interval_count' => $this->page['plan']->interval_count,
            'name'           => $this->page['plan']->name,
            'currency'       => 'GBP',
            'id'             => $this->page['plan']->id
        ];
        return \Stripe\Plan::create($array);
    }

    /**
     * Creates a stripe subscription for a customer with a plan
     * @param Customer Stripe Object
     * @param Rainlab\Users\Models\User Model
     * @param Plan Stripe Object
     */
    private function createStripeSubscription($customer, $user, $plan) {

        $plan = [
            "plan" => $plan->id
        ];

        $array = [
            'customer' => $customer->id,
            'items' => [
                $plan
            ],
            'trial_period_days' => $this->page['plan']->trial_period_days
        ];

        // if there's a deposit, override the trial period and set at one month
        // boolval intval as 0.00 doesn't == false but 0 does...
        if (boolval(intval($this->page['plan']->deposit))) {
            unset($array['trial_period_days']);
            $array['trial_end'] = strtotime("+1 month");
        }

        return \Stripe\Subscription::create($array);

    }

    /**
     * Charge the customer once
     * @param Price in pence
     * @param User
     * @param Plan Name
     */
    private function takeSinglePayment($amount, $user, $plan_name) {
        $charge = [
            "amount"        => $amount,
            "currency"      => "gbp",
            "customer"      => $user->customer_id,
            "description"   => $plan_name,
            "receipt_email" => $user->email
        ];
        // charge the customer once
        \Stripe\Charge::create($charge);
    }

    /**
     * Check the newly signed up to plan's category
     * to see if only one plan in this category can be active
     * if so, delete all other plans in this category against this user
     * @param User
     * @param Plan
     */
    private function checkOneActivePlanCategory(User $user, Plan $plan) {
        if (boolval($plan->category->one_active_plan)) {

            // get plans in category
            $plans = Plan::where('category_id', $plan->category->id)->pluck('id')->toArray();

            // get a list of plans the customer is currently subscribed to
            $user_plans = [];
            foreach ($user->plans as $user_plan) {
                // excluding the one they just signed up to
                if ($user_plan->id != $plan->id) {
                    $user_plans[] = $user_plan->id;
                }
            }

            // for each of the user's plan, if it's one of the plans to be cancelled, cancel.
            foreach ($user_plans as $user_plan) {
                if (in_array($user_plan, $plans)) {
                    $user->cancelPlan($user_plan);
                    $user->plans()->detach($user_plan);
                }
            }

        }
    }
}
