<?php namespace Monologophobia\Signup\Components;

use Lang;
use Auth;
use Flash;
use Stripe;
use Redirect;
use RainLab\User\Models\User;
use Cms\Classes\ComponentBase;
use Monologophobia\Signup\Models\Settings as SignupSettings;

/**
 * Payment Details component
 *
 * Shows a list of Stripe Subscriptions / Plans / Charges
 * for the customer along with the ability to change card payment details
 * 
 */

class PaymentDetails extends ComponentBase {

    public function componentDetails() {
        return [
            'name'        => 'monologophobia.signup::lang.components.paymentdetails.name',
            'description' => 'monologophobia.signup::lang.components.paymentdetails.description'
        ];
    }

    public function defineProperties() {
        return [
            'display' => [
                'title'   => 'monologophobia.signup::lang.components.paymentdetails.display',
                'type'    => 'dropdown',
                'options' => [
                    'overview'    => 'monologophobia.signup::lang.components.paymentdetails.options.overview',
                    'update_card' => 'monologophobia.signup::lang.components.paymentdetails.options.update_card',
                ]
            ]
        ];
    }

    public function onRun() {

        $user = Auth::getUser();
        try {
            if ($user) {
                $this->page['display'] = $this->property('display');
                if ($this->page['display'] == 'overview') {
                    // this is now loaded via ajax onRetrievePayments to improve page speed
                    //$this->page['payment_details'] = $user->retrievePayments();
                }
                else if ($this->page['display'] == 'update_card') {
                    // get correct stripe key for sandbox or live environment
                    $live = boolval(SignupSettings::get('live'));
                    $this->page['publishable_key'] = ($live ? SignupSettings::get('stripe_publishable') : SignupSettings::get('stripe_sandbox_publishable'));
                }
            }
        }
        catch (\Exception $e) {
            Flash::error($e->getMessage());
        }

    }

    public function onCancel() {
        try {
            $user = Auth::getUser();
            if ($user) {
                // delete automatically triggers Stripe Customer cancellation
                $user->forceDelete();
                Flash::success(Lang::get('monologophobia.signup::lang.components.messages.account_cancelled'));
                return Redirect::to('/');
            }
        }
        catch (\Exception $e) {
            Flash::error($e->getMessage());
            return Redirect::refresh();
        }
    }

    public function onUpdatePaymentDetails() {

        try {

            if ($user = Auth::getUser()) {

                // init stripe
                $live = boolval(SignupSettings::get('live'));
                $key  = ($live ? SignupSettings::get('stripe_secret') : SignupSettings::get('stripe_sandbox_secret'));
                \Stripe\Stripe::setApiKey($key);

                // get the customer and update the source with the supplied elements token
                $customer = \Stripe\Customer::retrieve($user->customer_id);
                $customer->source = post('token');
                $customer->save();
    
                Flash::success(Lang::get('monologophobia.signup::lang.components.messages.payment_details_updated'));
                return Redirect::back();

            }

        }
        catch (\Exception $e) {
            Flash::error($e->getMessage());
            return Redirect::refresh();
        }

    }

    public function onRetrievePayments() {
        try {
            $user = Auth::getUser();
            $payment_details = $user->retrievePayments();
            $array = [
                'subscriptions' => [],
                'payments'      => []
            ];
            foreach ($payment_details['subscriptions'] as $subscription) {
                $array['subscriptions'][] = $subscription;
            }
            foreach ($payment_details['payments'] as $payment) {
                $array['payments'][] = $payment;
            }
            return json_encode($array);
        }
        catch (\Exception $e) {
            Flash::error($e->getMessage());
        }
    }

}