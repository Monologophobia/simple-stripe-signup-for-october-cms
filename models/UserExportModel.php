<?php namespace Monologophobia\Signup\Models;

use BackendAuth;
use Backend\Models\ExportModel;
use RainLab\User\Models\User;

class UserExportModel extends ExportModel {

    public function exportData($columns, $sessionKey = null) {

        $users = User::get();

        $users->each(function($user) use ($columns) {
            $user->addVisible($columns);
            if (in_array('marketing_preferences_export', $columns)) {
                $user->marketing_preferences_export = $this->getMarketingPreferences($user);
            }
            if (in_array('plans_export', $columns)) {
                $user->plans_export = $this->getPlans($user);
            }
        });

        return $users->toArray();

    }

    private function getMarketingPreferences($user) {
        $return = '';
        $index  = 1;
        $count  = count($user->marketing_preferences);
        foreach ($user->marketing_preferences as $preference) {
            $return .= $preference->name;
            if ($index < $count) {
                $return .= '|';
                $index++;
            }
        }
        return $return;
    }

    private function getPlans($user) {
        $return = '';
        $index  = 1;
        $count  = count($user->plans);
        foreach ($user->plans as $plan) {
            $return .= $plan->name;
            if ($index < $count) {
                $return .= '|';
                $index++;
            }
        }
        return $return;
    }

}