<?php namespace Monologophobia\Signup\Models;

use \October\Rain\Database\Model;

class MarketingCategory extends Model {

    public $table = 'mono_signup_marketing_categories';

    public $timestamps = true;

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = ['name' => 'required|string'];

    public $belongsToMany = [
        'users' => [
            'RainLab\User\Models\User',
            'table'    => 'mono_signup_user_marketing_categories',
            'key'      => 'category_id',
            'otherKey' => 'user_id'
        ]
    ];

}
