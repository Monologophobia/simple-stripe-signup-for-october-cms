<?php namespace Monologophobia\Signup\Models;

use Lang;
use \October\Rain\Database\Model;

class Exercise extends Model {

    // The table to use
    public $table = 'mono_exercises';

    // Automatically generate created_at and updated_at
    public $timestamps = true;

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name'  => 'required|string',
        'video_link' => 'required|string'
    ];

    public $belongsToMany = [
        'exercise_sessions' => [
            'Monologophobia\Signup\Models\ExerciseSession',
            'table' => 'mono_exercise_sessions_exercises',
            'key' => 'exercise_id',
            'otherKey' => 'session_id'
        ]
    ];

    public function getExerciseIdOptions() {
        return $this->lists('name', 'id');
    }

}
