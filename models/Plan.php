<?php namespace Monologophobia\Signup\Models;

use Lang;
use \October\Rain\Database\Model;
use \October\Rain\Database\Traits\SoftDeleting;

use \RainLab\User\Models\User;

class Plan extends Model {

    // The table to use
    public $table = 'mono_signup_plans';

    // Automatically generate created_at and updated_at
    public $timestamps = true;
    // Add soft deleting to the model
    use SoftDeleting;
    protected $dates = ['deleted_at'];

    protected $nullable = ['training_dates'];
    protected $jsonable = ['training_files', 'training_dates'];

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name'                => 'required|string',
        'amount'              => 'required',
        'recurring_frequency' => 'required|integer',
        'interval_count'      => 'required|integer',
        'trial_period_days'   => 'required|integer'
    ];

    public $belongsTo = [
        'category' => ['Monologophobia\Signup\Models\Category', 'key' => 'category_id']
    ];

    public $belongsToMany = [
        'users' => [
            'RainLab\User\Models\User',
            'table'    => 'mono_users_plans',
            'key'      => 'plan_id',
            'otherKey' => 'user_id',
            'delete'   => true,
            'pivot'    => ['training_dates']
        ]
    ];

    public $morphMany = [
        'exercise_sessions' => ['Monologophobia\Signup\Models\ExerciseSession', 'name' => 'morphable']
    ];

    public $attachOne = [
        'image'    => ['System\Models\File'],
        'document' => ['System\Models\File']
    ];

    public function getRecurringFrequencyOptions() {
        return [
            0 => Lang::get('monologophobia.signup::lang.plans.recurring_frequency.none'),
            1 => Lang::get('monologophobia.signup::lang.plans.recurring_frequency.day'),
            2 => Lang::get('monologophobia.signup::lang.plans.recurring_frequency.week'),
            3 => Lang::get('monologophobia.signup::lang.plans.recurring_frequency.month'),
            4 => Lang::get('monologophobia.signup::lang.plans.recurring_frequency.year')
        ];
    }

    /**
     * Generates Stripe friendly recurring frequencies
     * 
     * @return string day, week, month, year, or boolean false
     */
    public function getRecurringFrequency() {
        // if it's marked as a one off payment (php 0 == false)
        if (!boolval($this->recurring_frequency)) return false;
        $stripe_friendly_names = [
            0 => false,
            1 => 'day',
            2 => 'week',
            3 => 'month',
            4 => 'year'
        ];
        return $stripe_friendly_names[$this->recurring_frequency];
    }

}
