<?php namespace Monologophobia\Signup\Models;

use \October\Rain\Database\Model;

class Category extends Model {

    // The table to use
    public $table = 'mono_signup_plans_categories';

    // explicitly remove timestamps
    public $timestamps = false;

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name' => 'required|string'
    ];

    public $hasMany = [
        'plans' => ['Monologophobia\Signup\Models\Plan', 'key' => 'category_id']
    ];

}
