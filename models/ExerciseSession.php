<?php namespace Monologophobia\Signup\Models;

use \October\Rain\Database\Model;

class ExerciseSession extends Model {

    // The table to use
    public $table = 'mono_exercise_sessions';

    // set up fields
    public $timestamps = true;
    protected $nullable = ['plan_id', 'user_id'];

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = ['day_date' => 'required'];

    public $morphTo = [
        'morphable' => []
    ];

    public $belongsToMany = [
        'exercises' => [
            'Monologophobia\Signup\Models\Exercise',
            'table' => 'mono_exercise_sessions_exercises',
            'key' => 'session_id',
            'otherKey' => 'exercise_id',
            'order' => 'sort_order asc',
            'pivot' => ['id', 'reps', 'sets', 'additional', 'sort_order'],
        ]
    ];

}
