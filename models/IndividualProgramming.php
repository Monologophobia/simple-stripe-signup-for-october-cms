<?php namespace Monologophobia\Signup\Models;

use \October\Rain\Database\Model;

class IndividualProgramming extends Model {

    // The table to use
    public $table = 'mono_users_individual_programming';

    // set up fields
    public $timestamps = true;
    protected $dates = ['active_date'];
    protected $nullable = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = ['active_date' => 'required|date'];

    public $belongsTo = [
        'user' => ['RainLab\User\Models\User', 'key' => 'user_id']
    ];

}
