<?php namespace Monologophobia\Signup\Models;

use Model;

class Settings extends Model {

    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'monologophobia_signup_settings';

    public $settingsFields = 'fields.yaml';

}
