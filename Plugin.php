<?php namespace Monologophobia\Signup;

use DB;
use Mail;
use Lang;
use Event;
use Crypt;

use Backend\Facades\Backend;
use Backend\Models\UserGroup;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;

use Monologophobia\Signup\Models\Settings as SignupSettings;

class Plugin extends PluginBase {

    // Requires the RainLab.Users plugin
    public $require = [
        'RainLab.User'
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails() {
        return [
            'name'        => Lang::get('monologophobia.signup::lang.plugin.name'),
            'description' => Lang::get('monologophobia.signup::lang.plugin.description'),
            'author'      => 'Monologophobia',
            'icon'        => 'icon-users'
        ];
    }

    /**
     * Register plugin permissions
     *
     * @return array
     */
    public function registerPermissions() {
        return [
            'monologophobia.signup.plans'    => ['tab' => Lang::get('monologophobia.signup::lang.plugin.name'), 'label' => Lang::get('monologophobia.signup::lang.permissions.settings')],
            'monologophobia.signup.settings' => ['tab' => Lang::get('monologophobia.signup::lang.plugin.name'), 'label' => Lang::get('monologophobia.signup::lang.permissions.staff')]
        ];
    }

    /**
     * Register plugin settings
     * 
     * @return array
     */
    public function registerSettings() {
        return [
            'settings' => [
                'label'       => Lang::get('monologophobia.signup::lang.settings.name'),
                'description' => Lang::get('monologophobia.signup::lang.settings.description'),
                'category'    => SettingsManager::CATEGORY_USERS,
                'icon'        => 'icon-money',
                'class'       => 'Monologophobia\Signup\Models\Settings',
                'order'       => 501,
                'permissions' => ['monologophobia.signup.settings']
            ]
        ];
    }

    /**
     * Register front end components
     * 
     * @return array
     */
    public function registerComponents() {
        return [
           '\Monologophobia\Signup\Components\Signup' => 'signup',
           '\Monologophobia\Signup\Components\PaymentDetails' => 'paymentdetails'
        ];
    }

    /**
     * The emails to send out from this plugin
     * 
     * @return array
     */
    public function registerMailTemplates() {
        return [
            'monologophobia.signup::signup.payment_failed'         => Lang::get('monologophobia.signup::lang.emails.payment_failed'),
            'monologophobia.signup::signup.subscription_cancelled' => Lang::get('monologophobia.signup::lang.emails.subscription_cancelled'),
            'monologophobia.signup::signup.user_cancelled'         => Lang::get('monologophobia.signup::lang.emails.user_cancelled')
        ];
    }

    /**
     * Implement Plugin extensions
     */
    public function boot() {

        // Extend User Model to have a plan via the pivot table
        // and a stripe customer ID via the same table
        // and add retrievePayments and cancelCustomer methods
        // for interacting with stripe
        \RainLab\User\Models\User::extend(function($model) {

            $model->belongsToMany['plans'] = [
                'Monologophobia\Signup\Models\Plan',
                'table'      => 'mono_users_plans',
                'key'        => 'user_id',
                'otherKey'   => 'plan_id',
                'timestamps' => true,
                'pivot'      => ['training_dates']
            ];

            $model->belongsToMany['marketing_preferences'] = [
                'Monologophobia\Signup\Models\MarketingCategory',
                'table'    => 'mono_signup_user_marketing_categories',
                'key'      => 'user_id',
                'otherKey' => 'category_id'
            ];

            $model->hasMany['individual_programming'] = [
                'Monologophobia\Signup\Models\IndividualProgramming', 'key' => 'user_id', 'delete' => true
            ];

            $model->morphMany['exercise_sessions'] = ['Monologophobia\Signup\Models\ExerciseSession', 'name' => 'morphable'];

            // encrypt name and surname
            $columns = ['name', 'surname'];
            $model->addFillable($columns);

            $model->bindEvent('model.afterFetch', function() use ($model, $columns) {
                foreach($columns as $column) {
                    try {
                        $model->$column = Crypt::decrypt($model->$column);
                    }
                    catch (\Illuminate\Contracts\Encryption\DecryptException $e) { }
                }
            });

            $model->addDynamicMethod('retrievePayments', function() use ($model) {
                try {
                    if ($model->customer_id) {
                        $this->setupStripe();
                        $customer = \Stripe\Customer::retrieve($model->customer_id);
                        if ($customer) {
                            return [
                                'subscriptions' => \Stripe\Subscription::all(array('customer' => $customer->id))->autoPagingIterator(),
                                'payments'      => \Stripe\Charge::all(array('customer' => $customer->id))->autoPagingIterator()
                            ];
                        }
                    }
                }
                catch (\Exception $e) {
                    \Log::error($e);
                    throw $e;
                }
            });

            $model->addDynamicMethod('cancelPlan', function($plan_id) use ($model) {
                try {
                    if ($model->customer_id) {
                        $this->setupStripe();
                        $customer = \Stripe\Customer::retrieve($model->customer_id);
                        if ($customer) {
                            $subscriptions = \Stripe\Subscription::all(array('customer' => $customer->id));
                            foreach ($subscriptions->autoPagingIterator() as $subscription) {
                                $sub = \Stripe\Subscription::retrieve($subscription->id);
                                if ($sub->plan->id == $plan_id) {
                                    $sub->cancel();
                                    break;
                                }
                            }
                        }
                    }
                }
                catch (\Exception $e) {
                    \Log::error($e);
                    throw $e;
                }
            });

            $model->addDynamicMethod('cancelStripeSubscriptions', function() use ($model) {
                try {
                    if ($model->customer_id) {
                        $this->setupStripe();
                        $customer = \Stripe\Customer::retrieve($model->customer_id);
                        if ($customer) {
                            $subscriptions = \Stripe\Subscription::all(array('customer' => $customer->id));
                            foreach ($subscriptions->autoPagingIterator() as $subscription) {
                                $sub = \Stripe\Subscription::retrieve($subscription->id);
                                $sub->cancel();
                            }
                        }
                    }
                }
                catch (\Exception $e) {
                    \Log::error($e);
                    throw $e;
                }
            });

            // I have moved this function to automatically call on customer delete
            // but left it here as a separate callable method in case you want to 
            // deactivate an account and cancel the customer but not completely remove
            // them from your system
            $model->addDynamicMethod('cancelStripeCustomer', function() use ($model) {
                try {
                    if ($model->customer_id) {
                        $this->setupStripe();
                        // to avoid a race condition with Stripe sending a customer.subscription.deleted
                        // event on their account deletion, remove their customer ID first
                        $customer_id = $model->customer_id;
                        if ($customer_id) {
                            $model->customer_id = false;
                            $model->save();
                            try {
                                $customer = \Stripe\Customer::retrieve($customer_id);
                                $customer->delete();
                            }
                            // if something goes wrong deleting the stripe customer
                            // restore the reference in the user model
                            catch (\Exception $e) {
                                \Log::error($e);
                                $model->customer_id = $customer_id;
                                $model->save();
                            }
                        }
                        return true;
                    }
                }
                catch (\Exception $e) {
                    \Log::error($e);
                    throw $e;
                }
            });

            /**
            * Takes the supplied marketing_preferences and inserts it via it's relationship
            * Removes all previous preferences first
            * @param Array Marketing Preferences by ID
            */
            $model->addDynamicMethod('updateMarketingPreferences', function(array $preferences) use ($model) {
                $model->marketing_preferences()->detach();
                // generate a nicely formatted array
                $insert = [];
                foreach ($preferences as $preference) {
                    $id = intval($preference);
                    if ($id) {
                        $insert[] = ['category_id' => $id];
                    }
                }
                $model->marketing_preferences()->attach($insert);
            });

            // on beforeDelete, delete Stripe Customer
            $model->bindEvent('model.beforeDelete', function() use ($model) {
                try {
                    $model->cancelStripeCustomer();
                }
                catch (\Exception $e) {
                    // do nothing. We still want to delete the customer so even
                    // if the stripe customer cancellation fails, log it (already done)
                    // and continue regardless
                }
            });

            $model->bindEvent('model.afterDelete', function() use ($model) {
                try {
                    // send email notification to customer
                    $vars = ['user' => $model, 'date' => Date('Y-m-d H:i:s')];
                    Mail::sendTo($model->email, 'monologophobia.signup::signup.user_cancelled', $vars);
                    // and to owners group
                    $group = UserGroup::where('code', 'owners')->first();
                    foreach ($group->users as $admin) {
                        Mail::sendTo($admin->email, 'monologophobia.signup::signup.user_cancelled', $vars);
                    }
                }
                catch (\Exception $e) {
                    // do nothing. We still want to delete the customer so even
                    // if the stripe customer cancellation fails, log it (already done)
                    // and continue regardless
                }
            });

        });

        // extend User Plugin to include a side menu 
        Event::listen('backend.menu.extendItems', function($manager) {
            $manager->addSideMenuItems('RainLab.User', 'user', [
                'users' => [
                    'label'       => 'rainlab.user::lang.users.menu_label',
                    'url'         => Backend::url('rainlab/user/users'),
                    'icon'        => 'icon-users',
                    'permissions' => ['rainlab.users.*'],
                    'order'       => 500,
                ],
                'plans' => [
                    'label'       => Lang::get('monologophobia.signup::lang.plans.name'),
                    'url'         => Backend::url('monologophobia/signup/plans'),
                    'icon'        => 'icon-money',
                    'permissions' => ['monologophobia.signup.plans'],
                    'order'       => 501,
                ],
                'exercises' => [
                    'label'       => 'Exercises',
                    'url'         => Backend::url('monologophobia/signup/exercises'),
                    'icon'        => 'icon-bicycle',
                    'permissions' => ['monologophobia.signup'],
                    'order'       => 502,
                ],
                'marketingpreferences' => [
                    'label'       => 'Marketing Preferences',
                    'url'         => Backend::url('monologophobia/signup/marketingpreferences'),
                    'icon'        => 'icon-list',
                    'permissions' => ['monologophobia.signup'],
                    'order'       => 503,
                ]
            ]);
        });

        \RainLab\User\Controllers\Users::extend(function($controller) {

            // override list config to update toolbar buttons
            $controller->listConfig = '~/plugins/monologophobia/signup/controllers/users/config_list.yaml';

            // Implement the relation controller if it doesn't exist already
            if (!$controller->isClassExtendedWith('Backend.Behaviors.RelationController')) {
                $controller->implement[] = 'Backend.Behaviors.RelationController';
            }

            // Define property if not already defined
            if (!isset($controller->relationConfig)) $controller->addDynamicProperty('relationConfig');

            // add new relation data
            $myConfigPath = '~/plugins/monologophobia/signup/controllers/users/config_relation.yaml';
            $controller->relationConfig = $controller->mergeConfig(
                $controller->relationConfig,
                $myConfigPath
            );

        });

        // Extend all backend list usage
        Event::listen('backend.list.extendColumns', function($widget) {
            if (
                !$widget->getController() instanceof \RainLab\User\Controllers\Users ||
                !$widget->model instanceof \RainLab\User\Models\User
            ) {
                return;
            }
            // Add a payment plan column
            $widget->addColumns([
                'plans' => [
                    'label'      => Lang::get('monologophobia.signup::lang.plans.name'),
                    'relation'   => 'plans',
                    'valueFrom'  => 'name',
                    'searchable' => 'true',
                    'sortable'   => 'true'
                ],
                'customer_id' => [
                    'label'      => Lang::get('monologophobia.signup::lang.user.fields.customer_id'),
                    'type'       => 'text',
                    'searchable' => 'true',
                    'sortable'   => 'true'
                ]
            ]);
        });

        // Extend all backend form usage
        Event::listen('backend.form.extendFields', function($widget) {

            // Only for the User controller and model
            // also prevent tabbed repeater loading all other fields
            if (
                !$widget->getController() instanceof \RainLab\User\Controllers\Users ||
                !$widget->model instanceof \RainLab\User\Models\User ||
                starts_with($widget->arrayName, "User[old_individual_programming]")
            ) {
                return;
            }

            // Add an payment plan field
            $widget->addFields([
                'customer_id' => [
                    'label' => Lang::get('monologophobia.signup::lang.user.fields.customer_id'),
                    'type'  => 'text',
                    'span'  => 'left'
                ],
                'terms_and_conditions' => [
                    'label' => Lang::get('monologophobia.signup::lang.user.fields.terms'),
                    'tab'   => Lang::get('monologophobia.signup::lang.user.fields.general'),
                    'type'  => 'checkbox',
                    'span'  => 'right'
                ],
            ]);

            $widget->addSecondaryTabFields([
                'payments' => [
                    'label' => Lang::get('monologophobia.signup::lang.user.fields.payments'),
                    'type'  => 'partial',
                    'path'  => '~/plugins/monologophobia/signup/controllers/users/_payments.htm'
                ]
            ]);

            $widget->addTabFields([
                'plans' => [
                    'label'    => Lang::get('monologophobia.signup::lang.plans.name'),
                    'tab'      => Lang::get('monologophobia.signup::lang.plans.name'),
                    'type'     => 'relation',
                    'relation' => 'plans',
                ],
                'training_dates' => [
                    'label'    => 'Training Dates',
                    'tab'      => Lang::get('monologophobia.signup::lang.plans.name'),
                    'type'     => 'partial',
                    'path'    => '~/plugins/monologophobia/signup/controllers/users/_training_dates.htm'
                ],
                'individual_programming' => [
                    'label'   => Lang::get('monologophobia.signup::lang.user.fields.individual_programming.name'),
                    'type'    => 'partial',
                    'tab'     => Lang::get('monologophobia.signup::lang.user.fields.individual_programming.name'),
                    'path'    => '~/plugins/monologophobia/signup/controllers/users/_individual_programming.htm'
                ],
                'marketing_preferences' => [
                    'label'    => 'Marketing Preferences',
                    'tab'      => 'Marketing',
                    'type'     => 'relation',
                    'relation' => 'marketing_preferences',
                ]
            ]);

        });

        // Extend User plugin filters
        Event::listen('backend.filter.extendScopes', function ($widget) {
            // Only for the User controller filters
            if (!$widget->getController() instanceof \RainLab\User\Controllers\Users) {
                return;
            }
            // Add an extra filter column scope
            // no longer supported with multiple plans
            /*$widget->addScopes([
                'plans' => [
                    'label'      => Lang::get('monologophobia.signup::lang.plans.name'),
                    'modelClass' => 'Monologophobia\Signup\Models\Plan',
                    'conditions' => 'plan_id in (:filtered)',
                    'nameFrom'   => 'name'
                ]
            ]);*/
        });

    }

    /**
     * Sets up Stripe with the correct API key
     * Checks if we're live and selects the correct secret key
     */
    private function setupStripe() {
        $live = boolval(SignupSettings::get('live'));
        $key  = ($live ? SignupSettings::get('stripe_secret') : SignupSettings::get('stripe_sandbox_secret'));
        \Stripe\Stripe::setApiKey($key);
    }

}


