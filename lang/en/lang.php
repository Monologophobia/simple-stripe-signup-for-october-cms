<?php

return [

    'plugin' => [
        'name'        => 'Signups',
        'description' => 'Extend the RainLab.User plugin to allow paid for Signups via Stripe'
    ],

    'permissions' => [
        'plans'    => 'Modify Payment Plans',
        'settings' => 'Alter Payment Settings (Stripe API Keys)'
    ],

    'settings' => [
        'name'        => 'Signup Settings',
        'description' => 'Alter Payment Settings (Stripe API Keys)',
        'fields' => [
            'live' => 'Is Live?',
            'publishable_key' => 'Publishable Key',
            'secret_key'      => 'Secret Key',
            'sandbox_publishable_key' => 'Sandbox Publishable Key',
            'sandbox_secret_key'      => 'Sandbox Secret Key',
            'webhook_secret'          => 'Webhook Signing Secret',
            'sandbox_webhook_secret'  => 'Sandbox Webhook Signing Secret'
        ]
    ],

    'user' => [
        'fields' => [
            'customer_id' => 'Stripe Customer ID',
            'payments'    => 'Payment Data',
            'individual_programming' => [
                'name' => 'Individual Programming',
                'active_date' => 'Active Date',
                'days' => [
                    'monday'    => 'Monday',
                    'tuesday'   => 'Tuesday',
                    'wednesday' => 'Wednesday', 
                    'thursday'  => 'Thursday',
                    'friday'    => 'Friday',
                    'saturday'  => 'Saturday',
                    'sunday'    => 'Sunday'
                ]
            ],
            'general' => 'General',
            'gender' => [
                'gender' => 'Gender',
                'male'   => 'Male',
                'female' => 'Female',
                'other'  => 'Other'
            ],
            'postcode' => 'Postcode',
            'dob'      => 'Date of Birth',
            'terms'    => 'Accepted Terms and Conditions',
        ]
    ],

    'plans' => [
        'name'     => 'Payment Plans',
        'singular' => 'Plan',
        'fields' => [
            'name'                => 'Name',
            'short_description'   => 'Short Description',
            'description'         => 'Description',
            'amount'              => 'Amount (£)',
            'recurring_frequency' => 'Recurring Frequency',
            'interval_count'      => 'Interval Count',
            'trial_period_days'   => 'Trial Period in Days',
            'image'               => 'Image',
            'document'            => 'Main Training Document',
            'order'               => 'Order',
            'training_files'      => 'Training Files',
            'weeks'               => 'Display File x weeks after signup',
            'training_file'       => 'File',
            'one_active_plan'     => 'Only allow one active plan per category',
            'one_active_plan_description' => 'If set, the system will only allow the customer to sign up to one plan that belongs to this category. On joining a new plan, all old plans in this category will be cancelled'
        ],
        'recurring_frequency' => [
            'none'  => 'One-off payment',
            'day'   => 'Daily',
            'week'  => 'Weekly',
            'month' => 'Monthly',
            'year'  => 'Yearly'
        ],
        'text' => [
            'deleted' => 'Plans Deleted',
            'new'     => 'New Plan',
            'saved'   => 'Plan Saved',
            'update'  => 'Edit Plan',
            'new_button'     => 'New',
            'delete_button'  => 'Delete',
            'confirm_delete' => 'Are you sure you want to delete these Payment Plans?'
        ]
    ],
    'categories' => [
        'name'     => 'Categories',
        'singular' => 'Category',
        'fields' => [
            'name' => 'Name',
        ],
        'text' => [
            'manage'  => 'Manage Categories',
            'deleted' => 'Category Deleted',
            'new'     => 'New Category',
            'saved'   => 'Category Saved',
            'update'  => 'Edit Category',
            'new_button'     => 'New',
            'delete_button'  => 'Delete',
            'confirm_delete' => 'Are you sure you want to delete these Categories? This will also delete all associated Payment Plans.'
        ]
    ],

    'components' => [
        'signup' => [
            'name' => 'Signup',
            'description' => 'Allows the customer to login and register with payment',
            'plan_id' => 'Plan ID'
        ],
        'paymentdetails' => [
            'name'        => 'Payment Details',
            'description' => 'Display a list of subscriptions and payments for the user. Also allows total account cancellation (Stripe customer as well)',
            'display'     => 'Display Type',
            'options'     => [
                'overview'    => 'Payments Overview',
                'update_card' => 'Update Card Details'
            ]
        ],
        'messages' => [
            'already_logged_in'       => 'Already Logged In',
            'plan_does_not_exist'     => 'Plan does not exist',
            'signup_successful'       => 'Signup successful',
            'account_cancelled'       => 'You have successfully cancelled your account',
            'payment_details_updated' => 'Your Payment Details have been updated'
        ]
    ],

    'emails' => [
        'payment_failed'         => 'Payment Failed',
        'subscription_cancelled' => 'Subscription Cancelled from multiple failed payments',
        'user_cancelled'         => 'User Cancellation'
    ]

];
